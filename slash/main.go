package main

import (
	"net/url"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
)

// Handler is our lambda handler invoked by the `lambda.Start` function call
func Handler(request events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {

	u, e := url.ParseQuery(request.Body)
	if e != nil {
		return events.APIGatewayProxyResponse{Body: "Try /fresco ping", StatusCode: 200}, nil
	}

	c := Command{RawText: u.Get("text")}
	c.Parse()
	responseText := c.Call()

	return events.APIGatewayProxyResponse{Body: responseText, StatusCode: 200}, nil

}

func main() {
	lambda.Start(Handler)
}

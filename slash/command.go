package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"strings"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/awserr"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
)

// Command to contain Fresco commands
type Command struct {
	RawText    string
	TeamDomain string
	Action     string
	Rest       string
}

// ActionData contains the data associated with an action
type ActionData struct {
	URL  string `json:"url"`
	Text string `json:"text"`
}

// FrescoPayload for posting to URL services
type FrescoPayload struct {
	Message string
}

// Parse incoming text
func (c *Command) Parse() {

	w := strings.Split(c.RawText, " ")

	c.Action = strings.ToLower(w[0])
	c.Rest = strings.Join(w[1:], " ")
}

// Call the URL by command keyword
func (c *Command) Call() string {

	a := getActions(c.Action)

	if a.Text != "" {
		return a.Text
	}

	if isValidURL(a.URL) {
		f := FrescoPayload{Message: c.Rest}
		jsonStr, _ := json.Marshal(f)
		//var jsonStr = []byte(`{"Message":"Buy cheese and bread for breakfast."}`)
		req, err := http.NewRequest("POST", a.URL, bytes.NewBuffer(jsonStr))
		req.Header.Set("X-Custom-Header", "myvalue")
		req.Header.Set("Content-Type", "application/json")

		client := &http.Client{}
		resp, err := client.Do(req)
		if err != nil {
			panic(err)
		}
		defer resp.Body.Close()

		body, _ := ioutil.ReadAll(resp.Body)

		fmt.Println(body)
		return string(body)

	}

	return "I don't understand what you're trying to do"
}

func getActions(keyword string) ActionData {

	var actions = make(map[string]ActionData)

	r, err := loadCommandsJSONFromS3()
	if err != nil {
		panic(err)
	}
	body, err := ioutil.ReadAll(r.Body)

	err = json.Unmarshal(body, &actions)
	if err != nil {
		fmt.Println("error:", err)
	}

	fmt.Printf("%s\n%v", keyword, actions)

	return actions[keyword]
}

func isValidURL(toTest string) bool {
	_, err := url.ParseRequestURI(toTest)
	if err != nil {
		return false
	}

	u, err := url.Parse(toTest)
	if err != nil || u.Scheme == "" || u.Host == "" {
		return false
	}

	return true
}

func loadCommandsJSONFromS3() (*s3.GetObjectOutput, error) {
	svc := s3.New(session.New())
	input := &s3.GetObjectInput{
		Bucket: aws.String("frsc.prd"),
		Key:    aws.String("commands.json"),
	}

	result, err := svc.GetObject(input)
	if err != nil {
		if aerr, ok := err.(awserr.Error); ok {
			switch aerr.Code() {
			case s3.ErrCodeNoSuchKey:
				fmt.Println(s3.ErrCodeNoSuchKey, aerr.Error())
			default:
				fmt.Println(aerr.Error())
			}
		} else {
			// Print the error, cast err to awserr.Error to get the Code and
			// Message from an error.
			fmt.Println(err.Error())
		}
		return nil, err
	}

	return result, err
}

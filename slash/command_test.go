package main

import "testing"

func TestParseText(t *testing.T) {
	c := Command{RawText: "ping"}
	c.Parse()

	if c.Action != "ping" {
		t.Errorf("Could not extract command")
	}
}

func TestActionURL(t *testing.T) {
	u := getActions("new")
	if u.URL != "https://" {
		t.Log(u)
		t.Errorf("Could not get action URL")
	}
}

func TestActionText(t *testing.T) {
	u := getActions("ping")
	if u.Text != "pong" {
		t.Log(u)
		t.Errorf("Action Ping Could not get action Pong")
	}
}

func TestCallText(t *testing.T) {
	c := Command{Action: "ping"}
	r := c.Call()
	if r != "pong" {
		t.Errorf("Action: Text doesn't seem to work")
	}
}

func TestCallGoodURL(t *testing.T) {
	c := Command{Action: "new"}
	r := c.Call()
	if r != "You reached new" {
		t.Errorf("New: didn't work")
	}
}

// func TestCallBadURL(t *testing.T) {
// 	c := Command{Action: "ping"}

// 	a := ActionData{URL: "pong"}

// }
// func TestCallFail(t *testing.T) {
// 	c := Command{Action: "ping"}

// 	a := ActionData{}

// }
